package com.adbrain.queue

import akka.actor.ActorLogging
import akka.camel.CamelMessage
import akka.camel.Consumer
import akka.actor.ActorRef

class HttpMessageListener(val queue: ActorRef, httpUrl:String) extends Consumer with ActorLogging {

  // Example of tcpUrl : 100.87.156.83:6200
 // val httpUrl = context.system.settings.config.getString("messageQueue.httpUrl")

  val endPoint = "jetty:http://%s".format(httpUrl)

  log.info("tcp endpoint =" + endPoint)

  def endpointUri = endPoint

  def receive = {
    case msg: CamelMessage => {
      if(msg.body != null)
         queue ! BidRequest(msg.bodyAs[String])
       else 
          queue ! BidRequest("msg is null")
          
          
      sender ! "1"    
       
    }
     
  }

}