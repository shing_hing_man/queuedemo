package com.adbrain.queue

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.event.Logging
import com.sun.xml.internal.txw2.Content

class MessageConsumer extends Actor with ActorLogging {

  val config = context.system.settings.config
  // val logger = Logging(context.system.eventStream, self.path.toString())

  // Get timeToProcess on message (in millsec) from application.conf
  val timeToProcessMsg = config.getInt("messageQueue.timeToProcessMsg")

  val fileSinKEnabled = config.getBoolean("messageQueue.fileSinkEnabled")
  val fileSink = context.actorFor("/user/fileSink")

  val path = self.path.toString()
  
  val msgGap = config.getInt("messageQueue.consumer.msgGap")
  log.info("%s msgGap=%s".format(path, msgGap))

  log.info("%s, timeToProcess one message in millsec=%s,fileSinkEnabled=%s".format(
    path, timeToProcessMsg, fileSinKEnabled))

  log.info("%s fileSink=%s".format(path, fileSink.path.toString()))

  var startTime = 0L

  var msgCount = 0

  def receive = {

    case bidMsg @ BidRequest(msg @ _) => {
      Thread.sleep(timeToProcessMsg);

      if (fileSinKEnabled) {
        fileSink ! bidMsg
      }

      logProcessTime

    }

  }

  def logProcessTime() {
    msgCount = msgCount + 1

    if (msgCount == 1) {
      startTime = System.currentTimeMillis()
    }

    if (msgCount >= msgGap) {
      val endTime = System.currentTimeMillis()
      val timeTaken = endTime - startTime
      log.info("%s, Time taken (in mills) to dispatch last %s msgs : %s".format(path, msgCount, timeTaken))
      startTime = endTime
      msgCount = 0
    }

  }

}