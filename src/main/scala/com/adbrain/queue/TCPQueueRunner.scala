package com.adbrain.queue

import akka.actor.ActorSystem
import akka.actor.Props
import akka.kernel.Bootable
import akka.routing.FromConfig
import akka.camel.CamelExtension
import scala.concurrent.Await
import akka.camel.Producer
import akka.actor.Actor
import akka.camel.Oneway
import org.apache.camel.Exchange

/**
 * Start queue to list for external tcp messages.
 */
class TCPQueueBootable extends Bootable {
  val runner = new TCPQueueRunner

  def startup() {
    runner.startup

  }

  def shutdown() {
    runner.shutdown
  }
}

class TCPQueueRunner {
  val system = ActorSystem("MessageQueueSystem")

  def startup(): Unit = {
    val config = system.settings.config

    val fileSink = system.actorOf(Props(new MessageFileSink), "fileSink")
    val messageQueue = system.actorOf(Props(new MessageQueue), "messageQueue")

    val tcpMessageListener = system.actorOf(Props(new TCPMessageListener(messageQueue)), "tcpListener")

    val bidRequestHttpUrl = system.settings.config.getString("messageQueue.bidRequestHttpUrl")
    val bidRequestHttpListener = system.actorOf(Props(new HttpMessageListener(messageQueue, bidRequestHttpUrl)), "bidRequestHttpListener")

    val jsCollectorHttpUrl = system.settings.config.getString("messageQueue.jsCollectorHttpUrl")
    val jsCollectorHttpListener = system.actorOf(Props(new HttpMessageListener(messageQueue, jsCollectorHttpUrl)), "jscCollectorHttpListener")

 

    val camel = CamelExtension(system)

    import scala.concurrent.duration._
    val activationFuture = camel.activationFutureFor(tcpMessageListener)(timeout = 3 seconds,
      executor = system.dispatcher)
    // Wait for Camel initialisation before proceed.
    Await.result(activationFuture, 4 seconds)

    val activationFuture2 = camel.activationFutureFor(bidRequestHttpListener)(timeout = 3 seconds,
      executor = system.dispatcher)
    // Wait for Camel initialisation before proceed.
    Await.result(activationFuture2, 4 seconds)
    
    val activationFuture3 = camel.activationFutureFor(jsCollectorHttpListener)(timeout = 3 seconds,
      executor = system.dispatcher)
    // Wait for Camel initialisation before proceed.
    Await.result(activationFuture2, 4 seconds)

  }

  def shutdown() {
    system.shutdown
  }
}

object TCPQueueRunnerLocal {

  def main(args: Array[String]): Unit = {

    val runner = new TCPQueueRunner
    runner.startup

    println("TCPQueueRunner started. Press Enter to shutdown.")
    val ln = readLine()

    println("Shutting down ..... ")

    runner.shutdown
    println("Shut down.")

  }

}

