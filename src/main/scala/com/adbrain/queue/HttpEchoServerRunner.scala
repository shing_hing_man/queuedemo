package com.adbrain.queue

import akka.actor.ActorSystem
import akka.camel.CamelExtension
import scala.concurrent.Await
import akka.actor.Props
import akka.actor.ActorLogging
import akka.camel.CamelMessage
import akka.camel.Consumer
import akka.actor.ActorRef
import akka.kernel.Bootable




class HttpEchoServerBootable extends Bootable {
  val runner = new HttpEchoServerRunner

  def startup() {
    runner.startup

  }

  def shutdown() {
    runner.shutdown
  }
}

class HttpEchoServerRunner {
  val system = ActorSystem("httpEchoServer")

  def startup() {

    val tcpListener = system.actorOf(Props[HttpMessageListenerSyn], "listener")

    val camel = CamelExtension(system)

    import scala.concurrent.duration._

    val activationFuture = camel.activationFutureFor(tcpListener)(timeout = 5 seconds,
      executor = system.dispatcher)
    // Wait for Camel initialisation before proceed.
    Await.result(activationFuture, 5 seconds)

    system.log.info("---Http  Echo Server started")

  }

  def shutdown() {
    system.shutdown()
  }
}

object HttpEchoServerRunnerLocal {

  def main(args: Array[String]): Unit = {

    val runner = new HttpEchoServerRunner
    runner.startup

    println("Http Echo server  started.  Press Enter to shutdown.")
    val ln = readLine()

    println("Shutting down ..... ")
    runner.shutdown
    println("Shut down.")

  }

}

class HttpMessageListenerSyn extends Consumer with ActorLogging {

  // Example of tcpUrl : 100.87.156.83:6200
  val httpUrl = context.system.settings.config.getString("messageQueue.httpUrl")

  val endPoint = "jetty:http://%s".format(httpUrl)

  log.info("tcp endpoint =" + endPoint)

  def endpointUri = endPoint

  def receive = {
    case camelMsg: CamelMessage => {

     
      if (camelMsg.body == null ){
        log.info("msg is null")
         sender ! "Got message: null msg" 
      } else {
         val msg = camelMsg.bodyAs[String]
         sender ! "Got message:" + msg
         log.info("Receive :" + msg)
      }
    }
  }

}