package com.adbrain.queue

import akka.actor.ActorSystem
import akka.actor.Props
import akka.kernel.Bootable
import akka.camel.Producer
import akka.actor.Actor
import akka.camel.Oneway
import akka.camel.CamelExtension
import scala.concurrent.Await
import akka.actor.ActorLogging
import org.apache.camel.Exchange

/**
 * Send http messages to a given url ( specified in application.conf)
 */
class HttpMsgProducerBootable extends Bootable {

  val runner = new TCPMsgProducerRunner

  def startup() {
     runner.startup

  }

  def shutdown() {
    runner.shutdown
  }
}



class HttpMsgProducerRunner {
  val system = ActorSystem("HttpMsgProducer")

  def startup() {
    val config = system.settings.config
    val noOfMsgs = config.getInt("messageQueue.noOfMsgs")

    val httpMsgProducer = system.actorOf(Props[HttpMessageProducer], "httpMsgProducer")

    val camel = CamelExtension(system)
   
    import scala.concurrent.duration._

    val activationFuture = camel.activationFutureFor(httpMsgProducer)(timeout = 5 seconds,
      executor = system.dispatcher)
    // Wait for Camel initialisation before proceed.
    Await.result(activationFuture, 5 seconds)

    val dummyMsg:String = List.fill(2100)("a").mkString("")

    
    httpMsgProducer ! "Start"
    
    (1 to noOfMsgs) foreach (n => httpMsgProducer ! "%s %s".format(n,dummyMsg))
    
    
   httpMsgProducer ! "End"
     
    system.log.info("--- Sent %s messages".format(noOfMsgs))

  }
  
  def shutdown() {
    system.shutdown()
  }
}



object HttpMsgProducerRunnerLocal {

  def main(args: Array[String]): Unit = {

    val runner = new HttpMsgProducerRunner
    runner.startup

    println("HttpMsgProducerRunner  started.  Press Enter to shutdown.")
    val ln = readLine()

    println("Shutting down ..... ")

    runner.shutdown
    println("Shut down.")

  }

}




class HttpMessageProducer extends Actor with Producer with Oneway with ActorLogging {

  // Example of tcpUrl : 100.87.156.83:6200
  val httpUrl = context.system.settings.config.getString("messageQueue.bidRequestHttpSendUrl")

  val endPoint = "jetty:http://%s".format(httpUrl)
  
  log.info("httpUrl=" +  httpUrl + ", tcp endpoint =" + endPoint)

  def endpointUri = endPoint

}








