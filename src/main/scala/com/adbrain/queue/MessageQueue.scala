package com.adbrain.queue

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.Props.apply
import akka.actor.actorRef2Scala
import akka.routing.RoundRobinRouter.apply
import akka.routing.FromConfig
import akka.routing.Broadcast
import akka.routing.Broadcast

trait QueueMsg

case class BidRequest(val msg: String) extends QueueMsg;

class MessageQueue extends Actor with ActorLogging {

  var startTime = 0L
  var msgCount = 0

  //val messageRouter = context.actorOf(Props[MessageConsumer].withRouter(RoundRobinRouter(4)), name = "workerRouter")

  val messageRouter = context.actorOf(Props[MessageConsumer].withRouter(FromConfig()),
    "messageRouter")

  val fileSink = context.actorFor("/user/fileSink")
  
  val config = context.system.settings.config
  val msgGap = config.getInt("messageQueue.router.msgGap")
  log.info("msgGap =" + msgGap)

  def receive = {

    case bidRequest @ BidRequest(msg @ _) => {
      messageRouter ! bidRequest
      log.debug("Size of msg = %s, first 100 chars:%s".format(msg.size, if (msg.size < 100) msg else msg.substring(0, 100)))
      logProcessTime
    }

  }

  def logProcessTime() {
    msgCount = msgCount + 1

    if (msgCount == 1) {
      startTime = System.currentTimeMillis()
    }

    if (msgCount >= msgGap) {
      val endTime = System.currentTimeMillis()
      val timeTaken = endTime - startTime
      log.info("Time taken (in mills) to dispatch last %s msgs : %s".format(msgCount, timeTaken))
      startTime = endTime
      msgCount = 0
    }

  }

}