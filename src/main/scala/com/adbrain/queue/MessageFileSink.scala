package com.adbrain.queue

import akka.actor.Actor
import java.io.FileWriter
import java.text.SimpleDateFormat
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import java.io.File
import akka.event.Logging
import java.util.GregorianCalendar
import java.util.concurrent.TimeUnit
import scala.concurrent.duration._ // For the Duration DSL
import scala.concurrent.ExecutionContext.Implicits.global


case object Flush 

class MessageFileSink extends Actor  {

  val config = context.system.settings.config
  val logger = Logging(context.system.eventStream, self.path.toString())

  // Absolute path of the output file.
  val outputDir = config.getString("messageQueue.outputdir")
  logger.info("message output dir=" + outputDir)

  val outputDirFile = new File(outputDir)
  if (!outputDirFile.exists()) {
    outputDirFile.mkdir()
  }

  val timePattern = "yyyy_MM_dd_HH_mm_ss";
  val dateFormatter = new SimpleDateFormat(timePattern)
  val fileSeparator = System.getProperty("file.separator")

  var fileName = generateFileName
  logger.info("At start up fileName=" + fileName)
  
  var buffer = new ListBuffer[String]()
  val bufferSize=1000
  
  val initialDelay  = 1000.milliseconds //Duration(100, TimeUnit.MILLISECONDS);
  val interval =   Duration(2, TimeUnit.SECONDS);
  val scheduler = context.system.scheduler;
  

  val flusher = scheduler.schedule(initialDelay, interval, self, Flush);
  
  override def receive = {


    case BidRequest(msg @ _) => {
      buffer += msg
      if (buffer.size > bufferSize)
      
      writeToFile()
     

    }
    
    case Flush => {      
      writeToFile()
      
    }

  }

  private def generateFileName: String = {
    val timestamp = dateFormatter.format((new GregorianCalendar).getTime())
    val fileName = "%s%s%s.txt".format(outputDir, fileSeparator, timestamp)
    fileName
  }

  private def writeToFile(): Unit = {
    val file = new File(fileName);

    if (!file.exists()) {
      file.createNewFile();
    }

    val append = true

    val fileWriter = new FileWriter(fileName, append)

    buffer.foreach(line => fileWriter.write(line + "\n"))
    fileWriter.close();

    buffer.clear
  }
  
  
}