package com.adbrain.queue

import akka.actor.ActorSystem
import akka.actor.Props
import akka.camel.Oneway
import akka.camel.Producer
import akka.kernel.Bootable

/**
 * Send messages from a local actor to the MessageQueue
 */
class LocalQueueBootable extends Bootable {

  val runner = new LocalQueueRunner

  def startup() {

    runner.startup
  }

  def shutdown() {
    runner.shutdown
  }
}



class LocalQueueRunner {
  val system = ActorSystem("MessageQueueSystem")
  def startup() {
    
    val fileSink = system.actorOf(Props(new MessageFileSink), "fileSink")
    val messageQueue = system.actorOf(Props(new MessageQueue), "messageQueue")

    val msgGenerator = system.actorOf(Props(new MsgGenerator(messageQueue)), "msgGenerator")

    msgGenerator ! StartMsgGeneration

  }
  def shutdown() {
    system.shutdown
  }

}



object LocalQueueRunnerLocal {

  def main(args: Array[String]): Unit = {

    val runner = new LocalQueueRunner
    runner.startup

    println("LocalQueueRunner started. Press Enter to shutdown.")
    readLine()

    println("Shutting down ..... ")

    runner.shutdown
    println("Shut down.")

  }

}