package com.adbrain.queue

import akka.actor.ActorLogging
import akka.camel.CamelMessage
import akka.camel.Consumer
import akka.actor.ActorRef

class TCPMessageListener(val queue: ActorRef) extends Consumer with ActorLogging {

  
  // Example of tcpUrl : 100.87.156.83:6200
  val tcpUrl = context.system.settings.config.getString("messageQueue.tcpUrl")
  
   //  textline=true means use text codec. sync=false means no reply. The default message is 1024 bytes!
   //( decoderMaxLineLength=0 or -1 does not seems to work)
  val endPoint = "mina:tcp://%s?textline=true&sync=false&decoderMaxLineLength=10000".format(tcpUrl) 
 
  log.info("tcp endpoint =" + endPoint)
  
  //  textline=true means use text codec. sync=false means no reply.
  def endpointUri = endPoint

  def receive = {
    case msg: CamelMessage => {

       if(msg.body != null)
         queue ! BidRequest(msg.bodyAs[String])
       else 
          queue ! BidRequest("msg is null")
       
    }
  }

}