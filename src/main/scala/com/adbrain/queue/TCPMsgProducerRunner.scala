package com.adbrain.queue

import akka.actor.ActorSystem
import akka.actor.Props
import akka.kernel.Bootable
import akka.camel.Producer
import akka.actor.Actor
import akka.camel.Oneway
import akka.camel.CamelExtension
import scala.concurrent.Await
import akka.actor.ActorLogging
import org.apache.camel.Exchange

/**
 * Send tcp messages to a given url ( specified in application.conf)
 */
class TCPMsgProducerBootable extends Bootable {

  val runner = new TCPMsgProducerRunner

  def startup() {
     runner.startup

  }

  def shutdown() {
    runner.shutdown
  }
}



class TCPMsgProducerRunner {
  val system = ActorSystem("TCPMsgProducer")

  def startup() {
    val config = system.settings.config
    val noOfMsgs = config.getInt("messageQueue.noOfMsgs")

    val tcpMsgProducer = system.actorOf(Props[TcpMessageProducer], "tcpMsgProducer")

    val camel = CamelExtension(system)
   
    import scala.concurrent.duration._

    val activationFuture = camel.activationFutureFor(tcpMsgProducer)(timeout = 5 seconds,
      executor = system.dispatcher)
    // Wait for Camel initialisation before proceed.
    Await.result(activationFuture, 5 seconds)

    val dummyMsg:String = List.fill(2100)("a").mkString("")

    
    tcpMsgProducer ! "Start"
    
    (1 to noOfMsgs) foreach (n => tcpMsgProducer ! "%s %s".format(n,dummyMsg))
    
    
    tcpMsgProducer ! "End"
     
    system.log.info("--- Sent %s messages".format(noOfMsgs))

  }
  
  def shutdown() {
    system.shutdown()
  }
}



object TCPMsgProducerRunnerLocal {

  def main(args: Array[String]): Unit = {

    val runner = new TCPMsgProducerRunner
    runner.startup

    println("TCPMsgProducerRunner  started.  Press Enter to shutdown.")
    val ln = readLine()

    println("Shutting down ..... ")

    runner.shutdown
    println("Shut down.")

  }

}




class TcpMessageProducer extends Actor with Producer with Oneway with ActorLogging {

  // Example of tcpUrl : 100.87.156.83:6200
  val tcpUrl = context.system.settings.config.getString("messageQueue.tcpSendUrl")

  //  textline=true means use text codec. sync=false means no reply.
  val endPoint = "mina:tcp://%s?textline=true&sync=false".format(tcpUrl)

  log.info("tcp endpoint =" + endPoint)

  def endpointUri = endPoint

}








