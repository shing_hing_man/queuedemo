package com.adbrain.queue;


import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef

case object StartMsgGeneration

class MsgGenerator(val queue:ActorRef) extends Actor with ActorLogging {

	 val config = context.system.settings.config
     val noOfMsgs = config.getInt("messageQueue.noOfMsgs")
  
	 val dummyMsg:String = List.fill(2100)("a").mkString("")
	
	 def receive ={    
	    case StartMsgGeneration => {
	      log.info("---- Start generating messages");   
	      
	     
	      
	      (1 to noOfMsgs).foreach( _ => queue ! BidRequest(dummyMsg))
	     
	     
	      
	      log.info("----  %s messages generated".format(noOfMsgs));   
	      
	    }
	    
	                               
	  }

}
